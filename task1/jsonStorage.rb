require 'json'

class JsonStorage 
    attr_reader :filePath, :parsedItems

    def initialize(filePath)
        @filePath = filePath
    end

    def readItems
        jsonText = File.read(self.filePath);
        if jsonText.length == 0
            parsedItems = { "items": [] };
        else
            parsedItems = JSON.parse(jsonText);
        end
        return parsedItems;
    end

    def writeItems(items)
        jsonText = JSON.stringify(items, nil, 4);
        File.write(self.filePath, JSON.dump(jsonText))
    end
end