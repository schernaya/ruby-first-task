require "test/unit"
require '../repositories/genreRepository.rb'

class CinemaTest < Test::Unit::TestCase

    def test_get_drama_id
      g = GenreRepository.new()
      assert_equal "68c08819-f82c-4fd3-9a66-89c94bfbd047", g.getGenre('Drama').id, "getGenre('Drama') should return an id \"68c08819-f82c-4fd3-9a66-89c94bfbd047\""
    end

    def test_get_comedy_id
      g = GenreRepository.new()
      assert_equal "8f08f68f-12c8-433a-ba99-a14f1aa44cd2", g.getGenre('Comedy').id, "getGenre('Comedy') should return an id \"8f08f68f-12c8-433a-ba99-a14f1aa44cd2\""
    end
end
