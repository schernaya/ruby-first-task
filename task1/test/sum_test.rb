require "test/unit"
require '../repositories/ticketRepository.rb'

class SumTest < Test::Unit::TestCase
    def test_sum
        t = TicketRepository.new()
        month = "6"
        year = "2021"
        assert_equal "190.0", t.getSum(month, year)
      end
end