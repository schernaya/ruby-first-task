require 'json'

class GenreRepository 
    attr_reader :filePath, :genres
    @@genres = []

    def initialize(filePath='./data/genres.json') # (json, "genres.json")
        @filePath = filePath
        fileText = File.read(filePath);
        @@genres = JSON.parse(fileText);
    end

    def save
        File.write(self.filePath, JSON.dump(@@genres))
    end

    def getGenres
        return @@genres
    end

    def getGenre(name)
        for genre in @@genres
            if genre["name"] == name
                return genre
            end
        end
        return nil
    end
            
    def addGenre(newGenre)
        if !newGenre.is_a?(Genre)
            raise "Cannot add as it is not a Genre"
        end
        for genre in @@genres
            if genre["name"] == newGenre.name
                raise "The genre is already added."
            end
        end
        @@genres << newGenre.as_hash
        self.save
    end

    def updateGenre(genreToUpdate)
        if !genreToUpdate.is_a?(Genre)
            raise "Cannot update as it is not a Genre."
        end
        genre = @@genres.find { |h| h['name'] == genreToUpdate.name }
        if genre 
            genre['description'] = genreToUpdate.description
            self.save
        else 
            raise "Cannot update as the genre hasn't been found."
        end
    end

    def deleteGenre(name)
        @@genres.delete_if { |h| h["name"] == name }
        if self.getGenre(name)
            raise "Cannot delete as the genre hasn't been found."
        end
        self.save
    end
end