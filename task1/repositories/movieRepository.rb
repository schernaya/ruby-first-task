require 'json'
require_relative '../models/movie'

class MovieRepository 
    attr_reader :filePath, :movies
    @@movies = []

    def initialize(filePath='./data/movies.json')
        @filePath = filePath
        fileText = File.read(filePath);
        @@movies = JSON.parse(fileText);
    end

    def save
        File.write(self.filePath, JSON.dump(@@movies))
    end

    def getMovies
        return @@movies
    end

    def getMovie(title)
        for movie in @@movies
            if movie["title"] == title
                return movie
            end
        end
        return nil
    end

    def addMovie(newMovie)
        if !newMovie.is_a?(Movie)
            raise "Cannot add as it is not a Movie."
        end
        for movie in @@movies
            if movie["title"] == newMovie.title
                raise "The movie is already added."
            end
        end
        @@movies << newMovie.as_hash
        self.save
    end

    def updateMovie(movieToUpdate)
        if !movieToUpdate.is_a?(Movie)
            raise "Cannot update as it is not a Movie."
        end
        movie = @@movies.find { |h| h['title'] == movieToUpdate.title }
        if movie 
            movie['duration'] = movieToUpdate.duration
            movie['genre'] = movieToUpdate.genre.as_hash
            movie['rating'] = movieToUpdate.rating
            self.save
        else 
            raise "Cannot update as the movie hasn't been found."
        end
    end

    def deleteMovie(title)
        @@movies.delete_if { |h| h["title"] == title }
        if self.getMovie(title)
            raise "Cannot delete as the movie hasn't been found."
        end
        self.save
    end
end