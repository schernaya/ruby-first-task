require 'json'
require 'date'

# require_relative './../models/ticket.rb'

class TicketRepository 
    attr_reader :filePath, :tickets
    @@tickets = []

    # def parse(fileText) 
    #     hashItems = JSON.parse(fileText)
    #     for item in hashItems
    #         @@tickets << Ticket(item["id"], item["movie"], item["user"], item["date"], item["price"])
    #     end
    # end

    def initialize(filePath='../data/tickets.json')
        @filePath = filePath
        fileText = File.read(filePath)
        @@tickets = JSON.parse(fileText)
    end

    def save
        File.write(self.filePath, JSON.dump(@@tickets))
    end

    def getTickets
        return @@tickets
    end

    def getTicket(ticketId)
        for ticket in @@tickets
            if ticket["id"] == ticketId
                return ticket
            end
        end
        return nil
    end

    def addTicket(newTicket)
        if !newTicket.is_a?(Ticket)
            raise "Cannot add as it is not a Ticket."
        end
        @@tickets << newTicket.as_hash
        self.save
    end

    def updateTicket(ticketToUpdate)
        # if !ticketToUpdate.is_a?(Ticket)
        #     raise "Cannot update as it is not a Ticket."
        # end
        for ticket in @@tickets
            puts ticket["id"], ticketToUpdate.id
            if ticket["id"] == ticketToUpdate.id
                ticket['movie'] = ticketToUpdate.movie.as_hash
                ticket['user'] = ticketToUpdate.user.as_hash
                ticket['date'] = ticketToUpdate.date
                ticket['price'] = ticketToUpdate.price
                self.save
                return ticket
            end
        end
        raise "Cannot update as the ticket hasn't been found."
    end

    def deleteTicket(id)
        @@tickets.delete_if { |h| h["id"] == id }
        if self.getTicket(id)
            raise "Cannot delete as the ticket hasn't been found."
        end
        self.save
    end

    def getSum(month, year)
        sum = 0
        for ticket in @@tickets
            date = Date.parse ticket["date"] 
            if date.month == month.to_i && date.year == year.to_i
                sum += ticket["price"]
            end
        end
        return sum
    end
end