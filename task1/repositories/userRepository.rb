require 'json'

class UserRepository 
    attr_reader :filePath, :users
    @@users = []

    def initialize(filePath='./data/users.json')
        @filePath = filePath
        fileText = File.read(filePath);
        @@users = JSON.parse(fileText);
    end

    def save
        File.write(self.filePath, JSON.dump(@@users))
    end

    def getUsers
        return @@users
    end

    def getUser(name)
        for user in @@users
            if user["name"] == name
                return user
            end
        end
        return nil
    end

    def addUser(newUser)
        if !newUser.is_a?(User)
            raise "Cannot add as it is not a User"
        end
        for user in @@users
            if user["name"] == newUser.name
                raise "The user is already added."
            end
        end
        @@users << newUser.as_hash
        self.save
    end

    def updateUser(userToUpdate)
        if !userToUpdate.is_a?(User)
            raise "Cannot update as it is not a User."
        end
        user = @@users.find { |h| h['name'] == userToUpdate.name }
        if user 
            user['phone'] = userToUpdate.phone.to_s
            self.save
        else 
            raise "Cannot update as the user hasn't been found."
        end
    end

    def deleteUser(name)
        @@users.delete_if { |h| h["name"] == name }
        if self.getUser(name)
            raise "Cannot delete as the user hasn't been found."
        end
        self.save
    end
end