require './repositories/genreRepository.rb'
require './repositories/movieRepository.rb'
require './repositories/userRepository.rb'
require './repositories/ticketRepository.rb'
require_relative '../view.rb'

class Cinema
    
    def initialize()
        @genreRepository = GenreRepository.new()
        @movieRepository = MovieRepository.new()
        @userRepository = UserRepository.new()
        @ticketRepository = TicketRepository.new()

        @view = View.new()
    end


    def menu(methods, methods_list)
      puts "Choose an option:"
      while true
        begin
          @view.show_numerated_array(methods_list)
          method_id = gets.chomp
          if method_id == "0"
            break
          end
          if method_id.nil? 
            raise "You need to enter action"
          end
          methods[method_id].call
        rescue StandardError => e
          puts "Rescued: #{e.inspect}"
          @view.show_error(e)
        end
      end
    end

    def show
      methods_list = ['exit', 'get genres', 'get movies', 'get users', 'get tickets', 
                      'get genre', 'get movie', 'get user', 'get ticket', 'get sum',]
      methods = {
        "1" => lambda { self.getGenres },
        "2" => lambda { self.getMovies },
        "3" => lambda { self.getUsers },
        "4" => lambda { self.getTickets },
        "5" => lambda { self.getGenre },
        "6" => lambda { self.getMovie },
        "7" => lambda { self.getUser },
        "8" => lambda { self.getTicket },
        "9" => lambda { self.getSum },
      }
      self.menu(methods, methods_list)
    end

    def getGenres
      genres = @genreRepository.getGenres
      @view.show(genres)
    end

    def getMovies
      movies = @movieRepository.getMovies
      @view.show(movies)
    end

    def getUsers
      users = @userRepository.getUsers
      @view.show(users)
    end

    def getTickets
      tickets = @ticketRepository.getTickets
      @view.show(tickets)
    end

    def getGenre
      name = @view.get_value('name')
      genre = @genreRepository.getGenre(name)
      @view.show(genre)
    end

    def getMovie
      title = @view.get_value('title')
      movie = @movieRepository.getMovie(title)
      @view.show(movie)
    end

    def getUser
      name = @view.get_value('name')
      user = @userRepository.getUser(name)
      @view.show(user)
    end

    def getSum
      puts "Enter month: "
      month = gets.chomp
      puts "Enter year: "
      year = gets.chomp
      sum = @ticketRepository.getSum(month, year)
      puts "\nThe sum of tickets is #{sum}."
    end
end