require 'securerandom'

class User
    attr_reader :id, :name, :phone
  
    def initialize(name, phone=nil)
      @id = SecureRandom.uuid
      @name = self.valid_name(name)
      @phone = self.valid_phone(phone)
    end
  
    def valid_name(name) 
      if !name.is_a?(String) 
        raise "The name should be String!"
      end
      if name.length == 0
        raise "The name should not be empty!"
      end
      return name
    end
  
    def valid_phone(phone) 
      if !phone.nil? && (!phone[0,4].include? "+380")
        raise "Incorrect phone number. It should be in format '+380'"
      end
      if !phone.nil? && phone.length != 13
        raise "Incorrect phone number. It should contain '+' and 12 numbers"
      end
      return phone
    end

    def phone=(value)
      @phone = self.valid_phone(value)
    end

    def as_hash()
      userHash = {}
      self.instance_variables.each {
          |userKey| 
          key = userKey.to_s.delete("@")
          value = self.instance_variable_get(userKey)
          if value == nil
              userHash[key] = ""
          else
              userHash[key] = value
          end
      }
      return userHash
    end
  end