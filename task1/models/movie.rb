require 'securerandom'

class Movie
    attr_reader :id, :title, :duration, :genre, :rating
  
    def initialize(title, duration, genre=nil, rating=nil)
      @id = SecureRandom.uuid
      @title = self.valid_title(title)
      @duration = self.valid_duration(duration)
      @genre = self.valid_genre(genre)
      @rating = self.valid_rating(rating)
    end
  
    def valid_title(title) 
      if !title.is_a?(String) 
        raise "The title should be String!"
      elsif title.length == 0
        raise "The title should not be empty!"
      end
      return title
    end
  
    def valid_duration(duration) 
      if !duration.is_a?(Integer) || duration.is_a?(Integer) && duration < 0
        raise "The duration should be in minutes!"
      end
      return duration
    end
  
    def valid_genre(genre) 
      if !genre.nil? && !genre.is_a?(Genre)
        raise "The genre should be an instance of Genre!"
      end
      return genre
    end
  
    def valid_rating(rating) 
      if !rating.nil? && !rating.is_a?(Float)
        raise "The rating should be float!"
      elsif !rating.nil? && !rating.between?(0, 10) 
        raise "The rating should be between 0 and 10!"
      end
      return rating
    end

    def duration=(value)
      @duration = self.valid_duration(value)
    end

    def genre=(value)
      @genre = self.valid_genre(value)
    end

    def rating=(value)
      @rating = self.valid_rating(value)
    end

    def show
      puts "Title: #{@name}, Duration: #{@description}, Genre: #{@genre}, Rating: #{@rating}"
    end

    def as_hash()
      newMovieHash = {}
      self.instance_variables.each {
          |movieKey| 
          key = movieKey.to_s.delete("@")
          value = self.instance_variable_get(movieKey)

          if value == nil
              newMovieHash[key] = ""
          elsif value.is_a?(Genre)
            newMovieHash[key] = value.as_hash
          else
            newMovieHash[key] = value
          end
      }
      return newMovieHash
    end
  end