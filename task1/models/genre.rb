require 'securerandom'

class Genre
    attr_reader :id, :name, :description

    def initialize(name, description=nil)
      @id = SecureRandom.uuid
      @name = self.valid_name(name)
      @description = self.valid_description(description)
    end

    def as_json() 
      if @description != nil
        descriptionInJson = @description
      else
        descriptionInJson = ""
      end
    {
      name: @name,
      description: descriptionInJson
    }
    end

    def as_hash()
      newGenreHash = {}
      self.instance_variables.each {
          |genreKey| 
          key = genreKey.to_s.delete("@")
          value = self.instance_variable_get(genreKey)
          if value == nil
              newGenreHash[key] = ""
          else
              newGenreHash[key] = value
          end
      }
      return newGenreHash
    end
    
    def description=(value)
      @description = self.valid_description(value)
    end

    def valid_name(name) 
      if !name.is_a?(String) 
        raise "The name should be String!"
      end
      if name.length == 0
        raise "The name should not be empty!"
      end
      return name
    end

    def valid_description(description) 
      if !description.nil? && !description.is_a?(String)
        raise "The description should be String!"
      end
      return description
    end

    def show
      puts "Name: #{@name}, Description: #{@description}"
    end
end