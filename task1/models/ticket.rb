require 'date'
require 'securerandom'

class Ticket
  attr_reader :id, :movie, :user, :date, :price

  def initialize(movie, user=nil, date=Date.today, price=nil)
    @id = SecureRandom.uuid
    @movie = self.valid_movie(movie)
    @user = self.valid_user(user) 
    @date = self.valid_date(date)
    @price = self.valid_price(price)
  end

  def valid_movie(movie) 
    if !movie.nil? && !movie.is_a?(Movie)
      raise "The movie should be an instance of Movie!"
    end
    return movie
  end

  def valid_user(user) 
    if !user.nil? && !user.is_a?(User)
      raise "The user should be an instance of User!"
    end
    return user
  end

  def valid_date(date) 
    if !DateTime.strptime(date.to_s, '%Y-%m-%d')
      raise "Incorrect date."
    end
    return date
  end

  def valid_price(price) 
    if !price.nil? && !price.is_a?(Float)
    raise "The price should be float!"
    end
    return price
  end

  def as_hash()
    ticketHash = {}
    self.instance_variables.each {
        |ticketKey| 
        key = ticketKey.to_s.delete("@")
        value = self.instance_variable_get(ticketKey)

        if value == nil && key == "price"
            ticketHash[key] = 0
        elsif value == nil
          ticketHash[key] = ""
        elsif value.is_a?(Movie) || value.is_a?(User)
          ticketHash[key] = value.as_hash
        else
          ticketHash[key] = value
        end
    }
    return ticketHash
  end

  def movie=(value)
    @movie = self.valid_movie(value)
  end

  def user=(value)
    @user = self.valid_user(value)
  end

  def date=(value)
    @date = self.valid_date(value)
  end

  def price=(value)
    @price = self.valid_price(value)
  end
end