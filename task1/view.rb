class View
    def get_value(target)
        puts "Enter " + target 
        return gets.chomp
    end

    def show(items)
        if items
            pp items
        else
            puts "No entities found!"
        end
    end

    def show_numerated_array(methods_list)
        num = 0
        puts ""
        for item in methods_list
          puts "#{num} - #{item}"
          num += 1
        end
      end

    def show_error(target, reason=nil)
        if reason
            puts "'" + target + "'", "has incorrect", reason
        else
            puts "'" + target.to_s + "'", "cannot be nil"
        end
    end
end